class Book
  attr_accessor :title

  LITTLE_WORDS = %w[the a an and of in].freeze

  def title
    words = @title.split(' ')
    titleized_words = words.map.with_index do |word, i|
      if i != 0 && LITTLE_WORDS.include?(word)
        word.downcase
      else
        word.capitalize
      end
    end
    titleized_words.join(' ')
  end
end
