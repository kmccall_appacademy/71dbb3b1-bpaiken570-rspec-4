# Contrived Temperature class
class Temperature
  def initialize(option = {})
    @hash = option
  end

  def self.from_celsius(temp)
    new(c: temp)
  end

  def self.from_fahrenheit(temp)
    new(f: temp)
  end

  def in_fahrenheit
    return trim(@hash[:f]) if @hash.key?(:f)
    trim(ctof(@hash[:c]))
  end

  def in_celsius
    return trim(@hash[:c]) if @hash.key?(:c)
    trim(ftoc(@hash[:f]))
  end

  def ftoc(far_temp)
    ((far_temp - 32.0) * 5.0 / 9.0).round(1)
  end

  def ctof(cel_temp)
    (cel_temp * 9.0 / 5.0 + 32.0).round(1)
  end

  def trim(num)
    i = num.to_i
    f = num.to_f
    i == f ? i : f
  end
end

# Celsius inherits from Temperature
class Celsius < Temperature
  def initialize(temp)
    super(c: temp)
  end
end

# Fahrenheit inherits from Temperature
class Fahrenheit < Temperature
  def initialize(temp)
    super(f: temp)
  end
end
