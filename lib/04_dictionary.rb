class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(word_or_hash)
    if word_or_hash.is_a?(Hash)
      @entries.merge!(word_or_hash)
    else
      @entries[word_or_hash] = nil
    end
  end

  def keywords
    @entries.keys.to_a.sort
  end

  def include?(word)
    entries.keys.include?(word)
  end

  def find(word)
    @entries.select { |k| k.include?(word) }
  end

  def printable
    string = ''
    keywords.each do |word|
      string.concat(%([#{word}] "#{@entries[word]}"\n))
    end
    string.chomp # removes last record seperator from string (\n)
  end
end
